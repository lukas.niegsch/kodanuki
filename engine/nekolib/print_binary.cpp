#include <cstdlib>
#include <cstdio>

/**
 * Printes the 8-bit number in binary.
 */
void print_binary(short input) {
	long long number  = input;
	long long simd_01 = 0x0101010101010101;
	long long simd_p2 = 0x0100804020100804;
	long long simd_48 = 0x3030303030303030;
	long long digits  = (simd_p2 * number | number >> 7);
	long long output  = (digits & simd_01) | simd_48;
	std::printf("%.8s\n", (char*) &output);
}

